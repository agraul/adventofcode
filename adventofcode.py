#!/usr/bin/python3

import argparse
import functools
import itertools
import math
import pprint
from collections import Counter, defaultdict, namedtuple
from statistics import fmean, median
from typing import Optional


def day1(filename):
    old_val = None
    increase_counter = 0
    with open(filename) as f:
        for line in f:
            new_val = int(line)
            if old_val is not None and new_val > old_val:
                increase_counter += 1
            old_val = new_val

    print(increase_counter)


def day1_advanced(filename):
    temp = []
    increase_counter = 0
    with open(filename) as f:
        for line in f:
            new_val = int(line)

            if len(temp) == 3:
                previous, current = (sum(temp), sum(temp[-2:]) + new_val)
                if previous < current:
                    increase_counter += 1
                temp.pop(0)
            temp.append(new_val)

    print(increase_counter)


def day2(filename):
    hpos = 0
    depth = 0
    with open(filename) as f:
        for line in f:
            direction, amount = line.strip().split()
            amount = int(amount)
            if direction == "forward":
                hpos += amount
            elif direction == "down":
                depth += amount
            else:
                depth -= amount
    print(hpos * depth)


def day2_advanced(filename):
    hpos = 0
    aim = 0
    depth = 0
    with open(filename) as f:
        for line in f:
            direction, amount = line.strip().split()
            amount = int(amount)
            if direction == "forward":
                hpos += amount
                depth += amount * aim
            elif direction == "down":
                aim += amount
            else:
                aim -= amount
    print(hpos * depth)


def day3(filename):
    bit_counts = [0 for _ in range(12)]
    with open(filename) as f:
        for line in f:
            for pos, val in enumerate(line.strip()):
                if int(val) == 1:
                    bit_counts[pos] += 1
                else:
                    bit_counts[pos] -= 1
    most_common_bits = (1 if x > 0 else 0 for x in bit_counts)
    gamma_str = "".join((str(x) for x in most_common_bits))
    epsilon_str = "".join((str(int(x) ^ 1) for x in gamma_str))
    print(int(gamma_str, 2) * int(epsilon_str, 2))


def day3_advanced(filename):
    with open(filename) as f:
        lines = list(map(str.strip, f.readlines()))

    def find_rating(candidates, co2=False):
        idx = 0
        while len(candidates) != 1:
            tmp = []

            count_1s = [x[idx] for x in candidates].count("1")
            needed_bit = 1 if count_1s >= len(candidates) / 2 else 0

            if co2:
                needed_bit ^= 1

            for line in candidates:
                if int(line[idx]) == needed_bit:
                    tmp.append(line)

            candidates = tmp
            idx += 1
        return int(candidates[0], 2)

    print(find_rating(lines) * find_rating(lines, co2=True))


def day4(filename):
    Position = namedtuple("Position", "x,y")

    def mark(board, number) -> Optional[Position]:
        for row_i, row in enumerate(board):
            for column_i, column in enumerate(row):
                if column[0] == number:
                    column[1] = True
                    return Position(row_i, column_i)

    def check_won(board, pos: Position):
        return (
            len([p for p in board[pos.x] if p[1] is True]) == 5
            or len([p for p in (row[pos.y] for row in board) if p[1] is True]) == 5
        )

    def sum_unmarked(board) -> int:
        return sum([p[0] for row in board for p in row if p[1] is False])

    boards = [[]]
    with open(filename) as f:
        drawn_nums = [int(num) for num in f.readline().split(",")]

        f.readline()  # skip first empty line
        for line in f:
            if line == "\n":
                boards.append([])
            else:
                boards[-1].append([[int(num), False] for num in line.strip().split()])

    for drawn in drawn_nums:
        for board in boards:
            pos = mark(board, drawn)
            if pos is not None and check_won(board, pos):
                print(sum_unmarked(board) * drawn)
                exit()


def day4_advanced(filename):
    Position = namedtuple("Position", "x,y")

    def mark(board, number) -> Optional[Position]:
        for row_i, row in enumerate(board):
            for column_i, column in enumerate(row):
                if column[0] == number:
                    column[1] = True
                    return Position(row_i, column_i)

    def check_won(board, pos: Position):
        return (
            len([p for p in board[pos.x] if p[1] is True]) == 5
            or len([p for p in (row[pos.y] for row in board) if p[1] is True]) == 5
        )

    def sum_unmarked(board) -> int:
        return sum([p[0] for row in board for p in row if p[1] is False])

    boards = [[]]
    with open(filename) as f:
        drawn_nums = [int(num) for num in f.readline().split(",")]

        f.readline()  # skip first empty line
        for line in f:
            if line == "\n":
                boards.append([])
            else:
                boards[-1].append([[int(num), False] for num in line.strip().split()])

    for drawn in drawn_nums:
        tmp = []
        for board in boards:
            pos = mark(board, drawn)
            if pos is None or not check_won(board, pos):
                tmp.append(board)
        if len(tmp) == 0:
            print(sum_unmarked(boards[0]) * drawn)
            break
        boards = tmp


def day5(filename):
    Point = namedtuple("Point", "x,y")
    grid = [[0 for _ in range(1000)] for _ in range(1000)]
    with open(filename) as f:
        for line in f:
            from_str, _, to_str = line.strip().partition("->")
            p1 = Point(*(int(i) for i in from_str.split(",")))
            p2 = Point(*(int(i) for i in to_str.split(",")))
            if p1.x == p2.x:
                for row in range(min(p1.y, p2.y), max(p1.y, p2.y) + 1):
                    grid[row][p1.x] += 1
            elif p1.y == p2.y:
                for col in range(min(p1.x, p2.x), max(p1.x, p2.x) + 1):
                    grid[p1.y][col] += 1
    print(len([p for row in grid for p in row if p >= 2]))


def day5_advanced(filename):
    Point = namedtuple("Point", "x,y")
    grid = [[0 for _ in range(1000)] for _ in range(1000)]
    with open(filename) as f:
        for line in f:
            from_str, _, to_str = line.strip().partition("->")
            p1 = Point(*(int(i) for i in from_str.split(",")))
            p2 = Point(*(int(i) for i in to_str.split(",")))

            dx = p2.x - p1.x
            dy = p2.y - p1.y

            distance = abs(dx or dy)
            for i in range(distance + 1):
                if dx > 0:
                    x = p1.x + i
                elif dx < 0:
                    x = p1.x - i
                else:
                    x = p1.x

                if dy > 0:
                    y = p1.y + i
                elif dy < 0:
                    y = p1.y - i
                else:
                    y = p1.y

                grid[y][x] += 1

    print(len([p for row in grid for p in row if p >= 2]))


def day6(filename, days=80):
    with open(filename) as f:
        counts = Counter((int(i) for i in f.read().strip().split(",")))

    for day in range(days):
        zeros = counts[0]
        for i in range(8):
            counts[i] = counts[i + 1]
        counts[6] += zeros
        counts[8] = zeros

    print(counts.total())


def day6_advanced(filename):
    day6(filename, days=256)


def day7(filename):
    with open(filename) as f:
        positions = [int(i) for i in f.read().strip().split(",")]

    meeting_point = median(positions)
    print(int(sum(abs(p - meeting_point) for p in positions)))


def day7_advanced(filename):
    with open(filename) as f:
        positions = [int(i) for i in f.read().strip().split(",")]

    def cost(a, b):
        d = abs(a - b)
        return sum(range(d + 1))

    mean = fmean(positions)
    # mean is a good guess, but not always perfect -> look at the two nearest
    # positions (integers)
    candidate_1, candidate_2 = math.floor(mean), math.ceil(mean)
    min_distance = min(
        int(sum(cost(p, candidate_1) for p in positions)),
        int(sum(cost(p, candidate_2) for p in positions)),
    )

    print(min_distance)


def day8(filename):
    count_1478 = 0
    with open(filename) as f:
        for line in f:
            _, output_values = line.strip().split("|")
            for value in output_values.split():
                if len(value) in (2, 3, 4, 7):
                    count_1478 += 1
    print(count_1478)


def day8_advanced(filename):
    segments_to_digits = {
        "abcefg": "0",
        "cf": "1",
        "acdeg": "2",
        "acdfg": "3",
        "bcdf": "4",
        "abdfg": "5",
        "abdefg": "6",
        "acf": "7",
        "abcdefg": "8",
        "abcdfg": "9",
    }
    total = 0
    with open(filename) as f:
        for line in f:
            patterns, output_values = [part.split() for part in line.strip().split("|")]
            patterns.sort(key=len)
            # code lengths positions after sorting
            # [ 2 3 4 5 5 5 6 6 6 7]
            one_wires = set(patterns[0])
            four_wires = set(patterns[2])
            seven_wires = set(patterns[1])
            segment_wire = {}
            segment_wire["a"] = seven_wires - one_wires

            # find g
            for pattern in (patterns[3], patterns[4], patterns[5]):
                if len(s := set(pattern) - four_wires - seven_wires) == 1:
                    segment_wire["g"] = s
                    break

            # find e
            for pattern in (patterns[3], patterns[4], patterns[5]):
                if (
                    len(
                        s := set(pattern) - seven_wires - four_wires - segment_wire["g"]
                    )
                    == 1
                ):
                    segment_wire["e"] = s
                    break

            # find b/d
            s1, s2, s3 = set(patterns[3]), set(patterns[4]), set(patterns[5])
            segment_wire["d"] = (s1 & s2 & s3) - segment_wire["a"] - segment_wire["g"]
            segment_wire["b"] = four_wires - one_wires - segment_wire["d"]

            # find f/c
            s1, s2, s3 = (
                set(patterns[6]),
                set(patterns[7]),
                set(patterns[8]),
            )
            segment_wire["f"] = (
                (s1 & s2 & s3)
                - segment_wire["a"]
                - segment_wire["g"]
                - segment_wire["b"]
            )
            segment_wire["c"] = one_wires - segment_wire["f"]

            # decode
            digits = []
            for output in output_values:
                translated = [
                    k for c in output for k, v in segment_wire.items() if c in v
                ]
                digits.append(segments_to_digits["".join(sorted(translated))])

            total += int("".join(digits))

    print(total)


def day9(filename):
    with open(filename) as f:
        locations = [[int(h) for h in line.strip()] for line in f.readlines()]

    risk_level = 0
    for y, line in enumerate(locations):
        for x, height in enumerate(line):
            top = 9 if y == 0 else locations[y - 1][x]
            bot = 9 if y == len(locations) - 1 else locations[y + 1][x]
            left = 9 if x == 0 else line[x - 1]
            right = 9 if x == len(line) - 1 else line[x + 1]

            if top > height < bot and left > height < right:
                risk_level += height + 1
    print(risk_level)


def day9_advanced(filename):
    Point = namedtuple("Point", "y,x")
    with open(filename) as f:
        locations = [[int(h) for h in line.strip()] for line in f.readlines()]

    def recurse_basin(basin, locations) -> set[Point]:
        checked_points = set()
        for point in basin:
            top = 9 if point.y == 0 else locations[point.y - 1][point.x]
            bot = (
                9 if point.y == len(locations) - 1 else locations[point.y + 1][point.x]
            )
            left = 9 if point.x == 0 else locations[point.y][point.x - 1]
            right = (
                9
                if point.x == len(locations[point.y]) - 1
                else locations[point.y][point.x + 1]
            )

            height = locations[point.y][point.x]
            if height < top < 9:
                checked_points.add(Point(y=point.y - 1, x=point.x))
            if height < bot < 9:
                checked_points.add(Point(y=point.y + 1, x=point.x))
            if height < right < 9:
                checked_points.add(Point(y=point.y, x=point.x + 1))
            if height < left < 9:
                checked_points.add(Point(y=point.y, x=point.x - 1))

        new_points = checked_points - basin
        if new_points:
            basin |= recurse_basin(new_points, locations)
        return basin

    basins = []
    for y, line in enumerate(locations):
        for x, height in enumerate(line):
            top = 9 if y == 0 else locations[y - 1][x]
            bot = 9 if y == len(locations) - 1 else locations[y + 1][x]
            left = 9 if x == 0 else line[x - 1]
            right = 9 if x == len(line) - 1 else line[x + 1]

            if top > height < bot and left > height < right:
                basin = set()
                if top != 9:
                    basin.add(Point(y=y - 1, x=x))
                if bot != 9:
                    basin.add(Point(y=y + 1, x=x))
                if right != 9:
                    basin.add(Point(y=y, x=x + 1))
                if left != 9:
                    basin.add(Point(y=y, x=x - 1))

                basin.add(Point(y=y, x=x))
                basin |= recurse_basin(basin, locations)
                basins.append(basin)

    first, second, third = (
        len(basin) for basin in (sorted(basins, key=len, reverse=True))[:3]
    )
    print(first * second * third)


def day10(filename):
    error_score = 0
    open_close_pairs = {
        "(": ")",
        "[": "]",
        "{": "}",
        "<": ">",
    }
    costs = {
        ")": 3,
        "]": 57,
        "}": 1197,
        ">": 25137,
    }
    with open(filename) as f:
        for line in f:
            parens_stack = []
            for c in line:
                if c in ["(", "[", "{", "<"]:
                    parens_stack.append(c)
                elif c in [")", "]", "}", ">"]:
                    last_opened = parens_stack.pop()
                    if open_close_pairs[last_opened] != c:
                        error_score += costs[c]
                        break
                elif c == "\n":
                    continue
    print(error_score)


def day10_advanced(filename):
    open_close_pairs = {
        "(": ")",
        "[": "]",
        "{": "}",
        "<": ">",
    }
    costs = {
        "(": 1,
        "[": 2,
        "{": 3,
        "<": 4,
    }
    scores = []
    with open(filename) as f:
        for line in f:
            score = 0
            corrupted = False
            parens_stack = []
            for c in line:
                if c in ["(", "[", "{", "<"]:
                    parens_stack.append(c)
                elif c in [")", "]", "}", ">"]:
                    last_opened = parens_stack.pop()
                    corrupted = open_close_pairs[last_opened] != c
                elif c == "\n":
                    while parens_stack:
                        last_opened = parens_stack.pop()
                        score = score * 5 + costs[last_opened]
                    if score:
                        scores.append(score)

                if corrupted:
                    break
    print(median(sorted(scores)))


def day11(filename):
    grid = []
    with open(filename) as f:
        for line in f:
            grid.append([int(level) for level in line.strip()])

    flash_counter = 0
    steps = 100
    for _ in range(steps):
        to_flash = []
        flashed = set()
        for y in range(10):
            for x in range(10):
                grid[y][x] += 1
                if grid[y][x] > 9:
                    to_flash.append((y, x))

        while to_flash:
            current_nine = to_flash.pop()
            if current_nine in flashed:
                continue

            flashed.add(current_nine)
            flash_counter += 1

            y, x = current_nine
            if y == 0:
                if x > 0:
                    grid[y][x - 1] += 1  # left
                    if grid[y][x - 1] > 9:
                        to_flash.append((y, x - 1))
                    grid[y + 1][x - 1] += 1  # bottom-left
                    if grid[y + 1][x - 1] > 9:
                        to_flash.append((y + 1, x - 1))
                if x < 9:
                    grid[y][x + 1] += 1  # right
                    if grid[y][x + 1] > 9:
                        to_flash.append((y, x + 1))
                    grid[y + 1][x + 1] += 1  # bottom-right
                    if grid[y + 1][x + 1] > 9:
                        to_flash.append((y + 1, x + 1))

                grid[y + 1][x] += 1  # bottom
                if grid[y + 1][x] > 9:
                    to_flash.append((y + 1, x))
            elif y == 9:
                if x > 0:
                    grid[y][x - 1] += 1  # left
                    if grid[y][x - 1] > 9:
                        to_flash.append((y, x - 1))
                    grid[y - 1][x - 1] += 1  # top-left
                    if grid[y - 1][x - 1] > 9:
                        to_flash.append((y - 1, x - 1))
                if x < 9:
                    grid[y][x + 1] += 1  # right
                    if grid[y][x + 1] > 9:
                        to_flash.append((y, x + 1))
                    grid[y - 1][x + 1] += 1  # top-right
                    if grid[y - 1][x + 1] > 9:
                        to_flash.append((y - 1, x + 1))

                grid[y - 1][x] += 1  # top
                if grid[y - 1][x] > 9:
                    to_flash.append((y - 1, x))
            else:
                if x > 0:
                    grid[y][x - 1] += 1  # left
                    if grid[y][x - 1] > 9:
                        to_flash.append((y, x - 1))
                    grid[y + 1][x - 1] += 1  # bottom-left
                    if grid[y + 1][x - 1] > 9:
                        to_flash.append((y + 1, x - 1))
                    grid[y - 1][x - 1] += 1  # top-left
                    if grid[y - 1][x - 1] > 9:
                        to_flash.append((y - 1, x - 1))
                if x < 9:
                    grid[y][x + 1] += 1  # right
                    if grid[y][x + 1] > 9:
                        to_flash.append((y, x + 1))
                    grid[y - 1][x + 1] += 1  # top-right
                    if grid[y - 1][x + 1] > 9:
                        to_flash.append((y - 1, x + 1))
                    grid[y + 1][x + 1] += 1  # bottom-right
                    if grid[y + 1][x + 1] > 9:
                        to_flash.append((y + 1, x + 1))

                grid[y + 1][x] += 1  # bottom
                if grid[y + 1][x] > 9:
                    to_flash.append((y + 1, x))

                grid[y - 1][x] += 1  # top
                if grid[y - 1][x] > 9:
                    to_flash.append((y - 1, x))

        for y, x in flashed:
            grid[y][x] = 0
    print(flash_counter)


def day11_advanced(filename):
    grid = []
    with open(filename) as f:
        for line in f:
            grid.append([int(level) for level in line.strip()])

    flash_counter = 0
    steps = 1000
    for step in range(1, steps + 1):
        to_flash = []
        flashed = set()
        for y in range(10):
            for x in range(10):
                grid[y][x] += 1
                if grid[y][x] > 9:
                    to_flash.append((y, x))

        while to_flash:
            current_nine = to_flash.pop()
            if current_nine in flashed:
                continue

            flashed.add(current_nine)
            flash_counter += 1

            y, x = current_nine
            if y == 0:
                if x > 0:
                    grid[y][x - 1] += 1  # left
                    if grid[y][x - 1] > 9:
                        to_flash.append((y, x - 1))
                    grid[y + 1][x - 1] += 1  # bottom-left
                    if grid[y + 1][x - 1] > 9:
                        to_flash.append((y + 1, x - 1))
                if x < 9:
                    grid[y][x + 1] += 1  # right
                    if grid[y][x + 1] > 9:
                        to_flash.append((y, x + 1))
                    grid[y + 1][x + 1] += 1  # bottom-right
                    if grid[y + 1][x + 1] > 9:
                        to_flash.append((y + 1, x + 1))

                grid[y + 1][x] += 1  # bottom
                if grid[y + 1][x] > 9:
                    to_flash.append((y + 1, x))
            elif y == 9:
                if x > 0:
                    grid[y][x - 1] += 1  # left
                    if grid[y][x - 1] > 9:
                        to_flash.append((y, x - 1))
                    grid[y - 1][x - 1] += 1  # top-left
                    if grid[y - 1][x - 1] > 9:
                        to_flash.append((y - 1, x - 1))
                if x < 9:
                    grid[y][x + 1] += 1  # right
                    if grid[y][x + 1] > 9:
                        to_flash.append((y, x + 1))
                    grid[y - 1][x + 1] += 1  # top-right
                    if grid[y - 1][x + 1] > 9:
                        to_flash.append((y - 1, x + 1))

                grid[y - 1][x] += 1  # top
                if grid[y - 1][x] > 9:
                    to_flash.append((y - 1, x))
            else:
                if x > 0:
                    grid[y][x - 1] += 1  # left
                    if grid[y][x - 1] > 9:
                        to_flash.append((y, x - 1))
                    grid[y + 1][x - 1] += 1  # bottom-left
                    if grid[y + 1][x - 1] > 9:
                        to_flash.append((y + 1, x - 1))
                    grid[y - 1][x - 1] += 1  # top-left
                    if grid[y - 1][x - 1] > 9:
                        to_flash.append((y - 1, x - 1))
                if x < 9:
                    grid[y][x + 1] += 1  # right
                    if grid[y][x + 1] > 9:
                        to_flash.append((y, x + 1))
                    grid[y - 1][x + 1] += 1  # top-right
                    if grid[y - 1][x + 1] > 9:
                        to_flash.append((y - 1, x + 1))
                    grid[y + 1][x + 1] += 1  # bottom-right
                    if grid[y + 1][x + 1] > 9:
                        to_flash.append((y + 1, x + 1))

                grid[y + 1][x] += 1  # bottom
                if grid[y + 1][x] > 9:
                    to_flash.append((y + 1, x))

                grid[y - 1][x] += 1  # top
                if grid[y - 1][x] > 9:
                    to_flash.append((y - 1, x))

        for y, x in flashed:
            grid[y][x] = 0

        if len(flashed) == 100:  # all octopussies flashed at once
            print(step)
            break


def day12(filename):
    connections = defaultdict(set)
    with open(filename) as f:
        for line in f:
            a, b = line.strip().split("-")
            if a == "end" or b == "start":
                a, b = b, a
            connections[a].add(b)
            if b != "end" and a != "start":
                connections[b].add(a)

    def visit_node(node: str, visited: set, path: list, all_paths: list):
        path.append(node)
        if node.islower():
            visited.add(node)

        if node == "end":
            all_paths.append(path.copy())
            visited.remove("end")
            path.pop()
            return

        for conn in connections[node] - visited:
            visit_node(conn, visited, path, all_paths)

        path.pop()
        if node.islower():
            visited.remove(node)

    paths = []
    for node in connections["start"]:
        visit_node(node=node, visited=set(), path=["start"], all_paths=paths)
    print(len(paths))


def day12_advanced(filename):
    connections = defaultdict(set)
    with open(filename) as f:
        for line in f:
            a, b = line.strip().split("-")
            if a == "end" or b == "start":
                a, b = b, a
            connections[a].add(b)
            if b != "end" and a != "start":
                connections[b].add(a)

    def visit_node(node: str, visits: Counter, path: list, all_paths: list):
        path.append(node)
        if node.islower():
            visits[node] += 1

        if node == "end":
            all_paths.append(path.copy())
            visits[node] -= 1
            path.pop()
            return

        for conn in connections[node]:
            if (
                conn.isupper()
                or visits[conn] == 0
                or all(count < 2 for count in visits.values())
            ):
                visit_node(conn, visits, path, all_paths)

        path.pop()
        visits[node] -= 1

    paths = []
    for node in connections["start"]:
        visit_node(node=node, visits=Counter(), path=["start"], all_paths=paths)
    print(len(paths))


def day13(filename):
    paper = [["." for _ in range(1500)] for _ in range(1000)]
    with open(filename) as f:
        for line in f:
            line = line.strip()
            if "," in line:
                x, y = [int(i) for i in line.split(",")]
                paper[y][x] = "#"
            elif line.startswith("fold"):
                len_prefix = len("fold along ")
                axis, fold_at_str = line[len_prefix:].split("=")
                fold_at = int(fold_at_str)
                break

    if axis == "x":
        for row in paper:
            for x, pos in enumerate(row[fold_at:]):
                if pos == "#":
                    row[fold_at - x] = "#"
        print(sum(row[:fold_at].count("#") for row in paper))
    else:
        for y, row in enumerate(paper[fold_at:]):
            for x, pos in enumerate(row):
                paper[fold_at - y][x] = "#"
        print(sum(row.count("#") for row in paper[:fold_at]))


def day13_advanced(filename):
    paper = [["." for _ in range(1500)] for _ in range(1000)]
    folds = []
    with open(filename) as f:
        for line in f:
            line = line.strip()
            if "," in line:
                x, y = [int(i) for i in line.split(",")]
                paper[y][x] = "#"
            elif line.startswith("fold"):
                len_prefix = len("fold along ")
                axis, fold_at_str = line[len_prefix:].split("=")
                folds.append((axis, int(fold_at_str)))

    last_x = 1500
    last_y = 1500
    for axis, fold_at in folds:
        if axis == "x":
            for row in paper[:last_y]:
                for x, pos in enumerate(row[fold_at:last_x]):
                    if pos == "#":
                        row[fold_at - x] = "#"
            last_x = fold_at
        else:
            for y, row in enumerate(paper[fold_at:last_y]):
                for x, pos in enumerate(row[:last_x]):
                    if pos == "#":
                        paper[fold_at - y][x] = "#"
            last_y = fold_at

    result = [row[:last_x] for row in paper[:last_y]]
    pprint.pp(result, compact=True, width=last_x * 6)


def day14(filename):
    insertion_table = {}
    with open(filename) as f:
        polymer = f.readline().strip()
        f.readline()  # skip empty line
        for line in f:
            line = line.strip()
            pair, to_insert = line[:2], line[-1]
            insertion_table[pair] = to_insert

    steps = 10
    for _ in range(steps):
        new_polymer = []
        for a, b in itertools.pairwise(polymer):
            to_insert = insertion_table[a + b]
            new_polymer.append(a)
            new_polymer.append(to_insert)
        new_polymer.append(polymer[-1])
        polymer = "".join(new_polymer)
    counts = list(sorted(Counter(polymer).values()))
    print(counts[-1] - counts[0])


def day14_advanced(filename):
    insertion_table = {}
    with open(filename) as f:
        polymer = f.readline().strip()
        f.readline()  # skip empty line
        for line in f:
            line = line.strip()
            pair, to_insert = line[:2], line[-1]
            insertion_table[pair] = to_insert

    @functools.cache
    def insert(a: str, b: str, counter: int = 1) -> Counter:
        c = insertion_table[a + b]
        if counter == 40:
            return Counter(c)
        else:
            return Counter(c) + insert(a, c, counter + 1) + insert(c, b, counter + 1)

    counter = Counter(polymer)
    for a, b in itertools.pairwise(polymer):
        counter += insert(a, b)

    counts = list(sorted(counter.values()))
    print(counts[-1] - counts[0])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--day", type=int, required=True)
    parser.add_argument("--advanced", action="store_true", default=False)
    parser.add_argument("--input", type=str, help="Input file name")
    args = parser.parse_args()

    function_name = f"day{args.day}"
    if args.advanced:
        function_name += "_advanced"

    try:
        func = locals()[function_name]
    except KeyError:
        print(f"Function {function_name} not defined")
    else:
        func(args.input or f"day{args.day}.txt")
